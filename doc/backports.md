# Backports

This document describes the process of how we *backport*
changes (mostly bug fixes) to previous releases.

In the GitLab Agent project we generally follow the overall
GitLab backporting guidelines. See the following resources:

- https://handbook.gitlab.com/handbook/engineering/releases/backports/
- https://docs.gitlab.com/ee/policy/maintenance.html

Specifically we follow the policy stated in the second link above:

> Our current policy is:
>
> - Backporting bug fixes for only the current stable release at any given time - see patch releases below.
> - Backporting security fixes to the previous two monthly releases in addition to the current stable release.
>   In some circumstances (outlined in patch releases below) we may address a security vulnerability
>   to the current stable release only or in the regular monthly release process, with no backports.

## Creating a backport MR

The process of creating a backport Merge Request is intentionally simple:

- Wait for approval of the change set targeting the default branch.
- Create a new branch from the stable branch the backport targets.
    - Use the following format for your new branch for discoverability: `backports/<MAJOR>-<MINOR>/<BRANCH NAME>`.
    - The stable branches have the format of `<MAJOR->-<MINOR>-stable`.
- Cherry-pick the change set to backport onto that newly created branch.
- Submit a Merge Request from your backport branch to the stable branch.
- Wait for approval and eventually merge. Release managers will take it from here.
  See [doc/releases.md](/doc/releases.md) for details.

This may be done for all versions the backport targets.
