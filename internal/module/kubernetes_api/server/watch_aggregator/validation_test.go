package watch_aggregator //nolint:stylecheck

import (
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
)

func TestValidation_Valid(t *testing.T) {
	tests := []testhelpers.ValidTestcase{
		{
			Name: "watch minimal",
			Valid: &WatchRequest{
				WatchId: "abc",
				Type:    "watch",
				WatchParams: &WatchParams{
					Version:  "v1",
					Resource: "pods",
				},
			},
		},
		{
			Name: "unwatch minimal",
			Valid: &WatchRequest{
				WatchId: "abc",
				Type:    "unwatch",
			},
		},
	}
	testhelpers.AssertValid(t, tests)
}

func TestValidation_Invalid(t *testing.T) {
	tests := []testhelpers.InvalidTestcase{
		{
			ErrString: "validation error:\n - watch_id: value length must be at least 1 bytes [string.min_bytes]\n - type: value must be in list [\"watch\", \"unwatch\"] [string.in]",
			Invalid:   &WatchRequest{},
		},
		{
			ErrString: "validation error:\n - watch_params is required for type == watch [watch_params_required_for_watch]\n - watch_id: value length must be at least 1 bytes [string.min_bytes]",
			Invalid: &WatchRequest{
				Type: "watch",
			},
		},
		{
			ErrString: "validation error:\n - watch_id: value length must be at least 1 bytes [string.min_bytes]",
			Invalid: &WatchRequest{
				Type: "unwatch",
			},
		},
	}
	testhelpers.AssertInvalid(t, tests)
}
