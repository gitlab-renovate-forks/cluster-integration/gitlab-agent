package server

import (
	"fmt"
	"time"

	"github.com/bufbuild/protovalidate-go"
	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kubernetes_api"
)

const (
	webSocketTokenRequestParamName = "kas-websocket-token-request" //nolint:gosec
	userIDSubjectPrefix            = "user-id"
)

type webSocketToken struct {
	validator        protovalidate.Validator
	jwtSigningMethod jwt.SigningMethod
	jwtSecret        []byte
}

func (t *webSocketToken) generate(url string, agentID, userID int64, impConfig *kubernetes_api.ImpersonationConfig) (string, error) {
	now := time.Now()
	claims := &WebSocketTokenClaims{
		// Registered Claims
		RegisteredClaimIssuer:    api.JWTKAS,
		RegisteredClaimSubject:   fmt.Sprintf("%s:%d", userIDSubjectPrefix, userID),
		RegisteredClaimAudience:  []string{api.JWTKAS},
		RegisteredClaimExpiresAt: now.Add(5 * time.Second).Unix(),
		RegisteredClaimNotBefore: now.Add(-5 * time.Second).Unix(),
		RegisteredClaimIssuedAt:  now.Unix(),

		// Custom Private Claims
		Endpoint:            url,
		AgentId:             agentID,
		ImpersonationConfig: impConfig,
	}
	token, err := jwt.NewWithClaims(t.jwtSigningMethod, claims).SignedString(t.jwtSecret)
	if err != nil {
		return "", err
	}
	return token, nil
}

func (t *webSocketToken) verify(token string, url string) (int64 /* agent id */, *kubernetes_api.ImpersonationConfig, error) {
	claims := &ValidatingWebSocketTokenClaims{Validator: t.validator, ValidForEndpoint: url}
	_, err := jwt.ParseWithClaims(
		token,
		claims,
		func(*jwt.Token) (any, error) {
			return t.jwtSecret, nil
		},
		jwt.WithValidMethods([]string{t.jwtSigningMethod.Alg()}),
		jwt.WithIssuer(api.JWTKAS),
		jwt.WithAudience(api.JWTKAS),
	)

	if err != nil {
		return 0, nil, err
	}

	return claims.AgentId, claims.ImpersonationConfig, nil
}
