package server

import (
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
)

const (
	defaultReceptiveAgentPollPeriod = 1 * time.Minute
)

func ApplyDefaults(config *kascfg.ConfigurationFile) {
	prototool.NotNil(&config.Agent)
	prototool.NotNil(&config.Agent.ReceptiveAgent)

	c := config.Agent.ReceptiveAgent
	prototool.Duration(&c.PollPeriod, defaultReceptiveAgentPollPeriod)
}
