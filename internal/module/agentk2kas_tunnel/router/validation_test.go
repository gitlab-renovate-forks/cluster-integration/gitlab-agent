package router

import (
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
)

func TestValidation_Valid(t *testing.T) {
	tests := []testhelpers.ValidTestcase{
		{
			Name: "minimal, host and zero port",
			Valid: &MultiURL{
				Scheme: "grpc",
				Host:   "example.com",
				Port:   0,
			},
		},
		{
			Name: "minimal, IP and max port",
			Valid: &MultiURL{
				Scheme: "grpc",
				Ip:     [][]byte{{1, 1, 1, 1}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}},
				Port:   0xFFFF,
			},
		},
	}
	testhelpers.AssertValid(t, tests)
}

func TestValidation_Invalid(t *testing.T) {
	tests := []testhelpers.InvalidTestcase{
		{
			ErrString: "validation error:\n - either ip or host must be set [ip_or_host_set]\n - scheme: value length must be at least 1 bytes [string.min_bytes]\n - port: value must be less than or equal to 65535 [uint32.lte]",
			Invalid: &MultiURL{
				Scheme: "",
				Ip:     nil,
				Host:   "",
				Port:   0xFFFF + 1,
			},
		},
		{
			ErrString: "validation error:\n - ip[0]: value must be a valid IP address [bytes.ip]",
			Invalid: &MultiURL{
				Scheme: "test",
				Ip:     [][]byte{{1, 1, 1}},
			},
		},
	}
	testhelpers.AssertInvalid(t, tests)
}
