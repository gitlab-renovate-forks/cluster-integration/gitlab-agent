package agent

import (
	"context"
	"math/rand/v2"
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_agent_registrar"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testlogger"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/entity"
	"go.uber.org/mock/gomock"
	"google.golang.org/grpc"
	"k8s.io/client-go/kubernetes/fake"
)

func TestModule_Run(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	ctrl := gomock.NewController(t)
	client := mock_agent_registrar.NewMockAgentRegistrarClient(ctrl)
	gomock.InOrder(
		client.EXPECT().
			Register(gomock.Any(), gomock.Any(), gomock.Any()).
			DoAndReturn(func(ctx context.Context, request *rpc.RegisterRequest, opts ...grpc.CallOption) (*rpc.RegisterResponse, error) {
				cancel()
				return &rpc.RegisterResponse{}, nil
			}),
		client.EXPECT().Unregister(gomock.Any(), gomock.Any()),
	)

	m := &module{
		Log:         testlogger.New(t),
		AgentMeta:   &entity.AgentMeta{KubernetesVersion: &entity.KubernetesVersion{}},
		InstanceID:  rand.Int64(),
		PollConfig:  testhelpers.NewPollConfig(0),
		Client:      client,
		KubeVersion: fake.NewSimpleClientset().Discovery(),
	}
	_ = m.Run(ctx, nil)
}
