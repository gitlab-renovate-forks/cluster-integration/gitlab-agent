package agent

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/flux/rpc"
	"google.golang.org/grpc"
)

//go:generate mockgen.sh -source "doc.go" -destination "mock_for_test.go" -package "agent"

//go:generate mockgen.sh -source "reconcile_trigger.go" -destination "mock_reconciler_trigger_for_test.go" -package "agent"

//go:generate mockgen.sh -source "client.go" -destination "mock_client_for_test.go" -package "agent"

//go:generate mockgen.sh -source "../rpc/rpc_grpc.pb.go" -destination "mock_rpc_for_test.go" -package "agent"

//go:generate mockgen.sh -source "module.go" -destination "mock_module_for_test.go" -package "agent"

// Workaround for https://github.com/uber-go/mock/issues/197.
type GitLabFlux_ReconcileProjectsClient interface { //nolint:stylecheck
	grpc.ServerStreamingClient[rpc.ReconcileProjectsResponse]
}
