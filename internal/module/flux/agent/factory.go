package agent

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	notificationv1 "github.com/fluxcd/notification-controller/api/v1"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/flux"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/flux/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	kubeerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/transport"
)

const (
	// resyncDuration defines the duration for the shared informer cache resync interval.
	resyncDuration = 10 * time.Minute

	reconcileProjectsInitBackoff   = 10 * time.Second
	reconcileProjectsMaxBackoff    = 5 * time.Minute
	reconcileProjectsResetDuration = 10 * time.Minute
	reconcileProjectsBackoffFactor = 2.0
	reconcileProjectsJitter        = 5.0

	conditionStatusTrue      = "True"
	conditionTypeEstablished = "Established"
)

var (
	requiredFluxCRDs = [...]schema.GroupVersionResource{
		sourcev1.GroupVersion.WithResource("gitrepositories"),
		notificationv1.GroupVersion.WithResource("receivers"),
	}
)

type Factory struct{}

func (f *Factory) IsProducingLeaderModules() bool {
	return true
}

func (f *Factory) New(config *modagent.Config) (modagent.Module, error) {
	restConfig, err := config.K8sUtilFactory.ToRESTConfig()
	if err != nil {
		return nil, err
	}

	restConfig = rest.CopyConfig(restConfig)
	restConfig.UserAgent = config.AgentNameVersion

	dynamicClient, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	if !isFluxInstalled(context.Background(), config.Log, dynamicClient) {
		config.Log.Info("Flux is not installed, skipping module. A restart is required for this to be checked again")
		return nil, nil
	}

	clientset, err := kubernetes.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	receiverClient := dynamicClient.Resource(notificationv1.GroupVersion.WithResource("receivers"))

	kubeAPIURL, _, err := rest.DefaultServerUrlFor(restConfig)
	if err != nil {
		return nil, err
	}
	transportCfg, err := restConfig.TransportConfig()
	if err != nil {
		return nil, err
	}
	kubeAPIRoundTripper, err := transport.New(transportCfg)
	if err != nil {
		return nil, err
	}

	config.RegisterKASAPI(&rpc.GitLabFlux_ServiceDesc)
	return &module{
		log: config.Log,
		runner: &fluxReconciliationRunner{
			informersFactory: func() (informers.GenericInformer, informers.GenericInformer, cache.Indexer) {
				informerFactory := dynamicinformer.NewDynamicSharedInformerFactory(dynamicClient, resyncDuration)
				gitRepositoryInformer := informerFactory.ForResource(sourcev1.GroupVersion.WithResource("gitrepositories"))
				receiverInformer := informerFactory.ForResource(notificationv1.GroupVersion.WithResource("receivers"))
				receiverIndexer := receiverInformer.Informer().GetIndexer()
				return gitRepositoryInformer, receiverInformer, receiverIndexer
			},
			clientFactory: func(ctx context.Context, cfgURL string, receiverIndexer cache.Indexer) (*client, error) {
				agentID, err := config.API.GetAgentID(ctx)
				if err != nil {
					return nil, err
				}

				rt, err := newGitRepositoryReconcileTrigger(cfgURL, kubeAPIURL, kubeAPIRoundTripper, http.DefaultTransport)
				if err != nil {
					return nil, err
				}

				return newClient(
					config.Log,
					config.API,
					agentID,
					rpc.NewGitLabFluxClient(config.KASConn),
					retry.NewPollConfigFactory(0, retry.NewExponentialBackoffFactory(
						reconcileProjectsInitBackoff,
						reconcileProjectsMaxBackoff,
						reconcileProjectsResetDuration,
						reconcileProjectsBackoffFactor,
						reconcileProjectsJitter,
					)),
					receiverIndexer,
					rt,
					clientset.CoreV1(),
				)
			},
			controllerFactory: func(ctx context.Context, gitRepositoryInformer informers.GenericInformer, receiverInformer informers.GenericInformer, projectReconciler projectReconciler) (controller, error) {
				agentID, err := config.API.GetAgentID(ctx)
				if err != nil {
					return nil, err
				}
				gitLabExternalURL, err := config.API.GetGitLabExternalURL(ctx)
				if err != nil {
					return nil, err
				}

				return newGitRepositoryController(ctx, config.Log, config.API, agentID, gitLabExternalURL, gitRepositoryInformer, receiverInformer, projectReconciler, receiverClient, clientset.CoreV1())
			},
		},
	}, nil
}

func (f *Factory) Name() string {
	return flux.ModuleName
}

func isFluxInstalled(ctx context.Context, log *slog.Logger, client dynamic.Interface) bool {
	for _, crd := range requiredFluxCRDs {
		ok, err := checkCRDExistsAndEstablished(ctx, client, crd)
		if err != nil {
			log.Error("Unable to check if CRD is installed", logz.K8sGroup(crd.Group), logz.Error(err))
			return false
		}
		if !ok {
			log.Debug("Required Flux CRD is not established", logz.K8sResource(crd.String()))
			return false
		}
	}
	return true
}

func checkCRDExistsAndEstablished(ctx context.Context, client dynamic.Interface, crd schema.GroupVersionResource) (bool, error) {
	crdGVR := schema.GroupVersionResource{
		Group:    "apiextensions.k8s.io",
		Version:  "v1",
		Resource: "customresourcedefinitions",
	}
	obj, err := client.Resource(crdGVR).Get(ctx, crd.GroupResource().String(), metav1.GetOptions{})
	if err != nil {
		if kubeerrors.IsNotFound(err) {
			return false, nil
		}
		return false, fmt.Errorf("unable to get CRD %q: %w", crd.String(), err)
	}

	if !isCRDVersionServed(obj, crd.Version) {
		return false, nil
	}

	return isCRDEstablished(obj), nil
}

func isCRDVersionServed(obj *unstructured.Unstructured, version string) bool {
	// Check if the appropriate API version of the CRD is served.
	versions, ok, err := unstructured.NestedSlice(obj.Object, "spec", "versions")
	if err != nil {
		return false
	}
	if !ok {
		return false
	}
	for _, v := range versions {
		v, ok := v.(map[string]any)
		if !ok {
			return false
		}
		name, ok := v["name"].(string)
		if !ok {
			return false
		}
		if name != version {
			continue
		}
		served, ok := v["served"].(bool)
		if !ok {
			return false
		}
		// we don't really care about any other conditions for now, because we don't own this CRD
		// and expect the owner to make sure it becomes established.
		return served
	}

	return false
}

func isCRDEstablished(obj *unstructured.Unstructured) bool {
	conditions, ok, err := unstructured.NestedSlice(obj.Object, "status", "conditions")
	if err != nil {
		return false
	}
	if !ok {
		return false
	}
	for _, c := range conditions {
		c, ok := c.(map[string]any)
		if !ok {
			continue
		}
		condType, ok := c["type"].(string)
		if !ok {
			continue
		}
		if condType != conditionTypeEstablished {
			continue
		}
		condStatus, ok := c["status"].(string)
		if !ok {
			return false
		}
		return condStatus == conditionStatusTrue
	}

	return false
}
