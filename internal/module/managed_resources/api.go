package managed_resources //nolint:stylecheck

import (
	"fmt"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration"
)

const (
	ModuleName          = "managed_resources"
	TemplatesDirectory  = "environment_templates"
	DefaultTemplateName = "default"
)

func EnvironmentTemplatesDirectoryForAgent(agentName string) string {
	return fmt.Sprintf("%s/%s", agent_configuration.DirectoryForAgent(agentName), TemplatesDirectory)
}

func EnvironmentTemplateFile(agentName, templateName string) string {
	return fmt.Sprintf("%s/%s.yaml", EnvironmentTemplatesDirectoryForAgent(agentName), templateName)
}
