package managed_resources //nolint:stylecheck

func (x *TemplatingInfo) ToEnvironmentInfo(tplName string) *EnvironmentInfo {
	return &EnvironmentInfo{
		EnvironmentId:      x.Environment.Id,
		EnvironmentName:    x.Environment.Name,
		EnvironmentSlug:    x.Environment.Slug,
		EnvironmentUrl:     x.Environment.Url,
		EnvironmentPageUrl: x.Environment.PageUrl,
		EnvironmentTier:    x.Environment.Tier,
		AgentId:            x.Agent.Id,
		AgentName:          x.Agent.Name,
		AgentUrl:           x.Agent.Url,
		ProjectId:          x.Project.Id,
		ProjectSlug:        x.Project.Slug,
		ProjectPath:        x.Project.Path,
		ProjectUrl:         x.Project.Url,
		TemplateName:       tplName,
	}
}
