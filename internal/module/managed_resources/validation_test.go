package managed_resources //nolint:stylecheck

import (
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
)

func TestValidation_Valid(t *testing.T) {
	tests := []testhelpers.ValidTestcase{
		{
			Name: "minimal Template",
			Valid: &EnvironmentTemplate{
				Name: "prod",
			},
		},
	}
	testhelpers.AssertValid(t, tests)
}

func TestValidation_Invalid(t *testing.T) {
	tests := []testhelpers.InvalidTestcase{
		//{
		//	ErrString: "",
		//	Invalid:   ,
		//},
	}
	testhelpers.AssertInvalid(t, tests)
}
