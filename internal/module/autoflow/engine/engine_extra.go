package engine

import "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/flow"

func toFlowModuleSources(ms []*ModuleSource) []flow.ModuleSource {
	fms := make([]flow.ModuleSource, len(ms))
	for i, m := range ms {
		fms[i] = m.toFlowModuleSource()
	}
	return fms
}

func (x *ModuleSource) toFlowModuleSource() flow.ModuleSource {
	return flow.ModuleSource{
		SourceName: x.SourceName,
		File:       x.File,
		Data:       x.Data,
	}
}

func fromFlowModuleSources(fms []flow.ModuleSource) []*ModuleSource {
	ms := make([]*ModuleSource, len(fms))
	for i, m := range fms {
		ms[i] = fromFlowModuleSource(m)
	}
	return ms
}

func fromFlowModuleSource(fm flow.ModuleSource) *ModuleSource {
	return &ModuleSource{
		SourceName: fm.SourceName,
		File:       fm.File,
		Data:       fm.Data,
	}
}

func toValuesMap(m map[string][]string) map[string]*Values {
	xs := make(map[string]*Values, len(m))
	for k, v := range m {
		xs[k] = &Values{Value: v}
	}
	return xs
}

func fromValuesMap(m map[string]*Values) map[string][]string {
	xs := make(map[string][]string, len(m))
	for k, v := range m {
		xs[k] = v.Value
	}
	return xs
}

func toFlowHTTPResponse(o *HttpDoActivityOutput) flow.HTTPResponse {
	return flow.HTTPResponse{
		Status:     o.Status,
		StatusCode: o.StatusCode,
		Header:     fromValuesMap(o.Header),
		Body:       o.Body,
	}
}
