package flow

import (
	"context"
	"fmt"

	"go.starlark.net/starlark"
)

var (
	_ Source = (*preloadedSource)(nil)
)

type ModuleSource struct {
	SourceName string
	File       string
	Data       []byte
}

type preloadedSource struct {
	modules []ModuleSource
}

func (p *preloadedSource) SetupThreadLocal(thread *starlark.Thread, projectPath string) {
}

func (p *preloadedSource) RecordSource(thread *starlark.Thread, name, projectPath, fullGitRef string) error {
	return nil
}

func (p *preloadedSource) LoadFile(ctx context.Context, thread *starlark.Thread, ref ModuleRef) ([]byte, error) {
	for _, module := range p.modules {
		if ref.SourceName == module.SourceName && ref.File == module.File {
			return module.Data, nil
		}
	}
	return nil, fmt.Errorf("unknown module: %s", ref)
}
