package tlstool

import (
	"crypto/tls"
	"fmt"
)

func ClientConfig() *tls.Config {
	return &tls.Config{
		CipherSuites: secureCipherSuites(),
		MinVersion:   tls.VersionTLS12,
	}
}

func ClientConfigWithCACert(caCertFile string) (*tls.Config, error) {
	tlsConfig := ClientConfig()
	if caCertFile != "" {
		certPool, err := LoadCACert(caCertFile)
		if err != nil {
			return nil, err
		}
		tlsConfig.RootCAs = certPool
	}
	return tlsConfig, nil
}

func ClientConfigWithCACertKeyPair(caCertFile, certFile, keyFile string) (*tls.Config, error) {
	tlsConfig, err := ClientConfigWithCACert(caCertFile)
	if err != nil {
		return nil, err
	}
	switch {
	case certFile != "" && keyFile != "":
		cert, err := tls.LoadX509KeyPair(certFile, keyFile)
		if err != nil {
			return nil, err
		}
		tlsConfig.Certificates = []tls.Certificate{cert}
	case certFile == "" && keyFile == "":
	// nothing to do
	default:
		return nil, fmt.Errorf("both certificate (%s) and key (%s) files must be specified", certFile, keyFile)
	}
	return tlsConfig, nil
}
