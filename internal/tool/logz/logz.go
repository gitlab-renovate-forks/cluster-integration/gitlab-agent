package logz

// Do not add more dependencies to this package as it's depended upon by the whole codebase.

import (
	"context"
	"encoding/base64"
	"fmt"
	"log/slog"
	"net"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

func NetAddressFromAddr(addr net.Addr) slog.Attr {
	return NetAddress(addr.String())
}

func NetNetworkFromAddr(addr net.Addr) slog.Attr {
	return NetNetwork(addr.Network())
}

func NetAddress(listenAddress string) slog.Attr {
	return slog.String("net_address", listenAddress)
}

func NetNetwork(listenNetwork string) slog.Attr {
	return slog.String("net_network", listenNetwork)
}

func IsWebSocket(isWebSocket bool) slog.Attr {
	return slog.Bool("is_websocket", isWebSocket)
}

func AgentID(agentID int64) slog.Attr {
	return slog.Int64(fieldz.AgentIDFieldName, agentID)
}

func CommitID(commitID string) slog.Attr {
	return slog.String("commit_id", commitID)
}

// ProjectID is the human-readable GitLab project path (e.g. gitlab-org/gitlab).
func ProjectID(projectID string) slog.Attr {
	return slog.String("project_id", projectID)
}

// ProjectIDN is the human-readable GitLab project numeric ID.
func ProjectIDN(projectID int64) slog.Attr {
	return slog.Int64("project_id_numeric", projectID)
}

func TraceIDFromContext(ctx context.Context) slog.Attr {
	return TraceID(trace.SpanContextFromContext(ctx).TraceID())
}

func TraceID(traceID trace.TraceID) slog.Attr {
	if !traceID.IsValid() {
		return slog.Attr{}
	}
	return slog.String("trace_id", traceID.String())
}

// Use for any keys in Redis.
func RedisKey(key []byte) slog.Attr {
	return LazyValue("redis_key", func() slog.Value {
		return slog.StringValue(base64.StdEncoding.EncodeToString(key))
	})
}

// Use for any integer counters.
func U64Count(count uint64) slog.Attr {
	return slog.Uint64("count", count)
}

// Use for any integer counters.
func TokenLimit(limit uint64) slog.Attr {
	return slog.Uint64("token_limit", limit)
}

func RemovedHashKeys(n int) slog.Attr {
	return slog.Int("removed_hash_keys", n)
}

// GitLab-kas or agentk module name.
func ModuleName(name string) slog.Attr {
	return slog.String("mod_name", name)
}

func GatewayURL(gatewayURL string) slog.Attr {
	return slog.String("gateway_url", gatewayURL)
}

func URLPathPrefix(urlPrefix string) slog.Attr {
	return slog.String("url_path_prefix", urlPrefix)
}

func URL(url string) slog.Attr {
	return slog.String("url", url)
}

func URLPath(url string) slog.Attr {
	return slog.String("url_path", url)
}

func GRPCService(service string) slog.Attr {
	return slog.String("grpc_service", service)
}

func GRPCMethod(method string) slog.Attr {
	return slog.String("grpc_method", method)
}

func VulnerabilitiesCount(n int) slog.Attr {
	return slog.Int("vulnerabilities_count", n)
}

func Error(err error) slog.Attr {
	return LazyValue("error", func() slog.Value {
		return slog.StringValue(err.Error())
	})
}

func WorkspaceName(name string) slog.Attr {
	return slog.String("workspace_name", name)
}

func WorkspaceNamespace(namespace string) slog.Attr {
	return slog.String("workspace_namespace", namespace)
}

func StatusCode(code int32) slog.Attr {
	return slog.Int64("status_code", int64(code))
}

func RequestID(requestID string) slog.Attr {
	return slog.String("request_id", requestID)
}

func DurationInMilliseconds(duration time.Duration) slog.Attr {
	return slog.Int64("duration_in_ms", duration.Milliseconds())
}

func PayloadSizeInBytes(size int) slog.Attr {
	return slog.Int("payload_size_in_bytes", size)
}

func WorkspaceDataCount(count int) slog.Attr {
	return slog.Int("workspace_data_count", count)
}

func ProtoJSONValue(key string, value proto.Message) slog.Attr {
	return LazyValue(key, func() slog.Value {
		data, err := protojson.Marshal(value)
		if err != nil {
			return slog.StringValue(fmt.Sprintf("failed to marshal proto: %v", err))
		}
		return slog.StringValue(string(data))
	})
}

func TargetNamespace(namespace string) slog.Attr {
	return slog.String("target_namespace", namespace)
}

func PodName(podName string) slog.Attr {
	return slog.String("pod_name", podName)
}

func PodNamespace(podNamespace string) slog.Attr {
	return slog.String("pod_namespace", podNamespace)
}

func PodStatus(podStatus string) slog.Attr {
	return slog.String("pod_status", podStatus)
}

func NamespacedName(n string) slog.Attr {
	return slog.String("namespaced_name", n)
}

func ProjectsToReconcile(p []string) slog.Attr {
	return Strings("projects_to_reconcile", p)
}

func GitRepositoryURL(url string) slog.Attr {
	return slog.String("gitrepository_url", url)
}

func ObjectKey(objKey string) slog.Attr {
	return slog.String("object_key", objKey)
}

func K8sGroup(groupName string) slog.Attr {
	return slog.String("k8s_group", groupName)
}

func K8sResource(resourceName string) slog.Attr {
	return slog.String("k8s_resource", resourceName)
}

func InventoryName(name string) slog.Attr {
	return slog.String("inventory_name", name)
}

func InventoryNamespace(namespace string) slog.Attr {
	return slog.String("inventory_namespace", namespace)
}

func K8sObjectName(name string) slog.Attr {
	return slog.String("object_name", name)
}

func K8sObjectNamespace(name string) slog.Attr {
	return slog.String("object_namespace", name)
}

func TunnelsByAgent(numTunnels int) slog.Attr {
	return slog.Int("tunnels_by_agent", numTunnels)
}

func FullReconciliationInterval(interval time.Duration) slog.Attr {
	return slog.Duration("full_reconciliation_interval", interval)
}

func PartialReconciliationInterval(interval time.Duration) slog.Attr {
	return slog.Duration("partial_reconciliation_interval", interval)
}

func LabelSelector(selector string) slog.Attr {
	return slog.String("label_selector", selector)
}

func WatchID(id string) slog.Attr {
	return slog.String("watch_id", id)
}

func Reason(reason string) slog.Attr {
	return slog.String("reason", reason)
}

func Msg(msg string) slog.Attr {
	return slog.String("msg", msg)
}

func ListenerName(name string) slog.Attr {
	return slog.String("listener_name", name)
}

func Attempt(attempt int) slog.Attr {
	return slog.Int("attempt", attempt)
}

func FlowScript(path string) slog.Attr {
	return slog.String("flow_script", path)
}

func EventID(id string) slog.Attr {
	return slog.String("event_id", id)
}

func EventType(typ string) slog.Attr {
	return slog.String("event_type", typ)
}

func EventHandler(name string) slog.Attr {
	return slog.String("event_handler", name)
}

func NumberOfEventHandlers(n int) slog.Attr {
	return slog.Int("number_of_event_handlers", n)
}

func IsScriptPrint() slog.Attr {
	return slog.Bool("is_script_print", true)
}
