package metric

import (
	"context"
	"fmt"
	"log/slog"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
)

func Register(registerer prometheus.Registerer, toRegister ...prometheus.Collector) error {
	for _, c := range toRegister {
		if err := registerer.Register(c); err != nil {
			return fmt.Errorf("registering %T: %w", c, err)
		}
	}
	return nil
}

type OtelErrorHandler slog.Logger

func (h *OtelErrorHandler) Handle(err error) {
	(*slog.Logger)(h).LogAttrs(context.Background(), slog.LevelWarn, "OpenTelemetry error", logz.Error(err))
}
