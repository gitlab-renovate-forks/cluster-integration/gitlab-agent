package redistool

import (
	"context"
	"errors"
	"testing"

	"github.com/redis/rueidis"
	"github.com/stretchr/testify/assert"
)

func TestDoReturnsErrorOnFailedEmptySet(t *testing.T) {
	b := redisIOBuilder[int, int]{
		err: errors.New("boom"),
	}

	assert.EqualError(t, b.Do(context.Background()), "boom")
}

func TestDoReturnsErrorOnFailedNonEmptySet(t *testing.T) {
	b := redisIOBuilder[int, int]{
		err:  errors.New("boom"),
		cmds: make([]rueidis.Completed, 1),
	}

	assert.EqualError(t, b.Do(context.Background()), "boom")
	assert.NoError(t, b.err)
	assert.Empty(t, b.cmds)
	assert.EqualValues(t, 1, cap(b.cmds))
}
