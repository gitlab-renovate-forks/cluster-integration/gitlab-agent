package grpctool

import (
	"net"
	"net/netip"
	"strconv"

	"google.golang.org/grpc/resolver"
	"google.golang.org/grpc/resolver/manual"
)

const (
	FixedAddressResolverScheme = "fixedaddr"
	FixedAddressResolverTarget = FixedAddressResolverScheme + ":///"
)

func NewFixedAddressResolver(addresses []netip.Addr, port uint16) *manual.Resolver {
	addrs := make([]resolver.Address, 0, len(addresses))
	portStr := strconv.FormatUint(uint64(port), 10)
	for _, uAddr := range addresses {
		addrs = append(addrs, resolver.Address{
			Addr: net.JoinHostPort(uAddr.String(), portStr),
		})
	}
	b := manual.NewBuilderWithScheme(FixedAddressResolverScheme)
	b.InitialState(resolver.State{
		Endpoints: []resolver.Endpoint{
			{
				Addresses: addrs,
			},
		},
	})
	return b
}
