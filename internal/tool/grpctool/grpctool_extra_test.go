package grpctool

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"k8s.io/utils/ptr"
)

func TestHTTPRequest_Header_IsRequestWithoutBody(t *testing.T) {
	testcases := []struct {
		name                  string
		contentLength         *int64
		expectedIsWithoutBody bool
	}{
		{
			name:                  "no content length is set, expecting body",
			contentLength:         nil,
			expectedIsWithoutBody: false,
		},
		{
			name:                  "content length is -1, expecting body",
			contentLength:         ptr.To(int64(-1)),
			expectedIsWithoutBody: false,
		},
		{
			name:                  "content length is 0, NOT expecting body",
			contentLength:         ptr.To(int64(0)),
			expectedIsWithoutBody: true,
		},
		{
			name:                  "content length is 1, expecting body",
			contentLength:         ptr.To(int64(1)),
			expectedIsWithoutBody: false,
		},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			// GIVEN
			header := HttpRequest_Header{
				ContentLength: tc.contentLength,
			}

			// WHEN
			isWithoutBody := header.IsRequestWithoutBody()

			// THEN
			assert.Equal(t, tc.expectedIsWithoutBody, isWithoutBody)
		})
	}
}
