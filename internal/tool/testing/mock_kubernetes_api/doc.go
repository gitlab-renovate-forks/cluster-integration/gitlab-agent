package mock_kubernetes_api

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"google.golang.org/grpc"
)

//go:generate mockgen.sh -source "../../../module/kubernetes_api/rpc/rpc_grpc.pb.go" -destination "rpc.go" -package "mock_kubernetes_api"

//go:generate mockgen.sh -source "doc.go" -destination "mock.go" -package "mock_kubernetes_api"

// Workaround for https://github.com/uber-go/mock/issues/197.
type KubernetesApi_MakeRequestClient interface {
	grpc.BidiStreamingClient[grpctool.HttpRequest, grpctool.HttpResponse]
}
