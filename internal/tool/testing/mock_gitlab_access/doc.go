// Mocks for GitLab access RPC.
package mock_gitlab_access

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"google.golang.org/grpc"
)

//go:generate mockgen.sh -source "../../../module/gitlab_access/rpc/rpc_grpc.pb.go" -destination "gitlab_access.go" -package "mock_gitlab_access"

//go:generate mockgen.sh -source "doc.go" -destination "mock.go" -package "mock_gitlab_access"

// Workaround for https://github.com/uber-go/mock/issues/197.
type GitlabAccess_MakeRequestClient interface {
	grpc.BidiStreamingClient[grpctool.HttpRequest, grpctool.HttpResponse]
}
