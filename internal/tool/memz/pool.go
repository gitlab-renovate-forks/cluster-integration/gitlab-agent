package memz

import (
	"google.golang.org/grpc/mem"
)

// 32KiB is what io.Copy() uses.
// It is also supported by the google.golang.org/grpc/mem package.
// https://github.com/grpc/grpc-go/blob/v1.66.0/mem/buffer_pool.go#L42

// Get32k returns a buffer that is at least 32KiB.
func Get32k() *[]byte {
	buf := mem.DefaultBufferPool().Get(32 * 1024)
	*buf = (*buf)[:cap(*buf)] // use all the capacity.
	return buf
}

func Put32k(b *[]byte) {
	mem.DefaultBufferPool().Put(b)
}
