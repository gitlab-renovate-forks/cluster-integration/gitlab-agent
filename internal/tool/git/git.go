package git

import (
	"strings"
)

const (
	branchRefPrefix = "refs/heads/"
	RefHEAD         = "HEAD"
)

func ExplicitRefOrHead(refName string) string {
	if refName == "" {
		return RefHEAD
	}
	if strings.HasPrefix(refName, branchRefPrefix) {
		return refName
	}
	return branchRefPrefix + refName
}
