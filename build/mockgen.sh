#!/usr/bin/env bash

# Make sure version matches go.mod
exec go run go.uber.org/mock/mockgen@v0.5.0 -typed "$@"
