# Dockerfile for agentk

# UBI versions from https://gitlab.com/gitlab-org/build/CNG/-/blob/master/ci_files/variables.yml
ARG BUILDER_IMAGE
ARG UBI_IMAGE=registry.access.redhat.com/ubi9/ubi-micro:9.5
ARG UBI_MINIMAL_IMAGE=registry.access.redhat.com/ubi9/ubi-minimal:9.5
ARG UID=1000

FROM ${BUILDER_IMAGE} AS builder

WORKDIR /src

COPY --chown=ci:ci . /src

RUN TARGET_DIRECTORY=. FIPS_MODE=1 make agentk

FROM ${UBI_IMAGE} AS target
FROM ${UBI_MINIMAL_IMAGE} AS packages

ENV DNF_INSTALL_ROOT=/install-root
ENV DNF_OPTS_ROOT="--installroot=${DNF_INSTALL_ROOT} --setopt=reposdir=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ --setopt=cachedir=/install-cache/ --setopt=varsdir= --config= --noplugins"

RUN mkdir -p "${DNF_INSTALL_ROOT}"
COPY --from=target   / "${DNF_INSTALL_ROOT}/"

# Perform `microdnf upgrade` now, and install any necessary packages into the chroot
RUN microdnf ${DNF_OPTS_ROOT} upgrade --nodocs --best --assumeyes --setopt=install_weak_deps=0 \
    && microdnf ${DNF_OPTS_ROOT} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 \
      openssl-libs \
    && microdnf clean all \
    && rm -f "${DNF_INSTALL_ROOT}/var/lib/dnf/history*"

FROM ${UBI_IMAGE}

# copy from packages, now updated and including packages required
COPY --from=packages /install-root/ /

LABEL source="https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent" \
      name="GitLab Agent for Kubernetes" \
      maintainer="GitLab group::environments" \
      vendor="GitLab" \
      summary="GitLab Agent for Kubernetes" \
      description="GitLab Agent for Kubernetes allows to integrate your cluster with GitLab in a secure way"
USER ${UID}

COPY --from=builder /src/agentk /usr/bin/agentk

ENTRYPOINT ["/usr/bin/agentk"]
