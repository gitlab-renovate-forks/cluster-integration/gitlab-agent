load("@bazel_tools//tools/build_defs/repo:cache.bzl", "DEFAULT_CANONICAL_ID_ENV", "get_default_canonical_id")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "update_attrs")

_FETCH_BUILD_BAZEL = """
load("@rules_proto//proto:defs.bzl", "proto_library")

proto_library(
    name = "proto",
    srcs = ["{file_name}"],
    strip_import_prefix = "{strip_import_prefix}",
    deps = {deps},
    tags = {tags},
    visibility = ["//visibility:public"],
)
"""

_common_attrs = {
    "urls": attr.string_list(mandatory = True),
    "integrity": attr.string(),
    "file_name": attr.string(default = "file.proto"),
    "strip_import_prefix": attr.string(),
    "deps": attr.string_list(),
}

_fetch_file_repo_attrs = _common_attrs

def _fetch_file_repo_impl(rctx):
    download_info = rctx.download(
        rctx.attr.urls,
        output = rctx.attr.file_name,
        canonical_id = get_default_canonical_id(rctx, rctx.attr.urls),
        integrity = rctx.attr.integrity,
    )
    build_file = _FETCH_BUILD_BAZEL.format(
        file_name = rctx.attr.file_name,
        strip_import_prefix = rctx.attr.strip_import_prefix,
        deps = str(rctx.attr.deps),
        tags = str(rctx.attr.tags),
    )
    rctx.file("BUILD.bazel", build_file)

    overrides = {"integrity": download_info.integrity, "tags": rctx.attr.tags}
    return update_attrs(rctx.attr, _fetch_file_repo_attrs.keys(), overrides)

_fetch_file_repo = repository_rule(
    implementation = _fetch_file_repo_impl,
    attrs = _common_attrs,
    environ = [DEFAULT_CANONICAL_ID_ENV],  # this attr is deprecated but get_default_canonical_id still uses rctx.os.environ.get() and not rctx.getenv()
)

_fetch = tag_class(
    attrs = _common_attrs | {
        "name": attr.string(mandatory = True),
        "tags": attr.string_list(),
    },
)

def _protofile_impl(mctx):
    root_direct_deps = []
    root_direct_dev_deps = []
    for mod in mctx.modules:
        for fetch in mod.tags.fetch:
            kwargs = {
                key: getattr(fetch, key)
                for key in _common_attrs.keys()
                if hasattr(fetch, key)
            }
            _fetch_file_repo(
                name = fetch.name,
                tags = fetch.tags,
                **kwargs
            )
            deps = root_direct_dev_deps if mctx.is_dev_dependency(fetch) else root_direct_deps
            deps.append(fetch.name)

    return mctx.extension_metadata(
        root_module_direct_deps = root_direct_deps,
        root_module_direct_dev_deps = root_direct_dev_deps,
        reproducible = True,
    )

protofile = module_extension(
    implementation = _protofile_impl,
    tag_classes = {
        "fetch": _fetch,
    },
    environ = [DEFAULT_CANONICAL_ID_ENV],
)
