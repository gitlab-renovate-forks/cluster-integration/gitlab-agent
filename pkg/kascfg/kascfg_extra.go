package kascfg

func (x *GitLabCF) ExternalURL() string {
	if x.ExternalUrl != nil {
		return *x.ExternalUrl
	}
	return x.Address
}

func (x *ListenPrivateApiCF) IsTLSEnabled() bool {
	return x.CertificateFile != "" && x.KeyFile != ""
}
